/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{



	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	class Player : public GameObject {
		shared_ptr< StateMachine<Player> >  m_StateMachine;	//ステートマシーン
															//移動の向きを得る
		Vector3 GetAngle();
		//最高速度
		float m_MaxSpeed;
		//減速率
		float m_Decel;
		//質量
		float m_Mass;

		float m_MoveBase;//移動距離の基準

		Vector3 m_BeforePos;//プレイヤーの前のポジション
		
		bool m_isReset;//前位置を更新するか否か

		vector<Vector3>odorVec; //匂いを残すポジションを格納
		vector<Vector3>::iterator odorItr;
	public:
		//構築と破棄
		Player(const shared_ptr<Stage>& StagePtr);
		virtual ~Player() {}
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr< StateMachine<Player> > GetStateMachine() const {
			return m_StateMachine;
		}
		//モーションを実装する関数群
		//移動して向きを移動方向にする
		void MoveRotationMotion();
		//Aボタンでジャンプするどうかを得る
		//bool IsJumpMotion();
		//Aボタンでジャンプする瞬間の処理
		//void JumpMotion();
		//Aボタンでジャンプしている間の処理
		//ジャンプ終了したらtrueを返す
		//bool JumpMoveMotion();
		//更新
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		//ターンの最終更新時
		virtual void OnLastUpdate() override;

		bool DropOdorFasible();//匂いを残すかどうかの判断
		void DropOdor();//匂いを残す処理
		
		//void BeforeReset();

		void NewBeforePos();//現在位置を更新

		Vector3 GetBeginOdor();//一番古い匂いを返す関数

		Vector3 GetBeforeLength();//補完距離を返す関数

		void DeleteOdor();//匂いを消す関数

		

	};

	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	class DefaultState : public ObjState<Player>
	{
		DefaultState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<DefaultState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

	


}
//end basecross


#pragma once
#include "stdafx.h"

namespace basecross {
	//-----------------------
	//犬
	//-----------------------
	class Dog :public GameObject {
		float m_MaxSpeed;	//最高速度
		Vector3 Odorlength; //匂いとの補完距離

		shared_ptr< StateMachine<Dog> >  m_StateMachine;//伝説のスーパーステートマシン

	public:
		Dog(const shared_ptr<Stage>& stage);//コンストラクタ
		~Dog() {};	//デストラクタ

		//ステートマシンのアクセサ
		shared_ptr<StateMachine<Dog>> GetStateMachine() const {
			return m_StateMachine;
		}

		virtual void OnUpdate() override;	//アップデート
		virtual void OnLastUpdate() override;//ラストアップデート
		virtual void OnCreate() override;

		Vector3 OdorPos();	//匂いのポジションを返す関数
		void ChaceOdor();//匂いを追いかける関数

		Vector3 GetLength();//匂いとの補完距離を返す関数

		void DeleteDOdor();
	};

	

	//--------------------------------
	//伝説のスーパーステートマシン
	//でふぉると
	//--------------------------------
	class DefaultDogState :public ObjState<Dog>
	{
		DefaultDogState() {};
	public:
		static shared_ptr<DefaultDogState> Instance();//インスタンス
		virtual void Enter(const shared_ptr<Dog>& obj)override;//初期
		virtual void Execute(const shared_ptr<Dog>& obj)override;//更新
		virtual void Exit(const shared_ptr<Dog>& obj)override;//脱出
	};
}

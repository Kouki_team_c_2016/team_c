/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {
	Dog::Dog(const shared_ptr<Stage>& stage):
		GameObject(stage),
		m_MaxSpeed(2.0f)
	{}

	void Dog::OnCreate()
	{
		//トランスフォーム初期設定
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(0.2f, 0.2f, 0.2f);
		PtrTrans->SetPosition(0.0f, 0.125f, -1.5f);
		PtrTrans->SetRotation(0, 0, 0);

		//Rigidbody
		auto PtrRigid = AddComponent<Rigidbody>();

		PtrRigid->SetReflection(0.5f);

		//重力
		auto PtrGravity = AddComponent<Gravity>();

		//最下地点
		PtrGravity->SetBaseY(0.125f);
		//衝突判定をつける
		auto PtrCollision = AddComponent<CollisionObb>();
		////横部分の反発
		//PtrCollision->SetIsHitAction(IsHitAction::AutoOnObjectRepel);
		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();
		//横部分のみ反発
		PtrCol->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		//かげのメッッッ…シュ…!!
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		//テクスチャをつける
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"TEMP_TX");
		SetAlphaActive(true);

		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
		
		m_StateMachine = make_shared<StateMachine<Dog>>(GetThis<Dog>());
		m_StateMachine->ChangeState(DefaultDogState::Instance());
	}

	//アップデート
	void Dog::OnUpdate()
	{
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
		
	}

	void Dog::ChaceOdor()
	{
		
		auto PtrTrans = GetComponent<Transform>();//トランスフォーム

		auto PtrPos = PtrTrans->GetPosition();
		auto time = App::GetApp()->GetElapsedTime();

		GetLength().Normalize();
		PtrTrans->SetPosition(PtrPos+(GetLength()*time*m_MaxSpeed));


	}

	//ラストアップデート
	void Dog::OnLastUpdate()
	{
		auto Pos = OdorPos();
		wstring PositionStr(L"Position:\t");
		PositionStr += L"X=" + Util::FloatToWStr(Pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		PositionStr += L"Y=" + Util::FloatToWStr(Pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		PositionStr += L"Z=" + Util::FloatToWStr(Pos.z, 6, Util::FloatModify::Fixed) + L"\n";
		wstring str = PositionStr;

		////デバッグ用文字をつける
		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetText(str);
	}

	//匂いを察知する関数
	Vector3 Dog::OdorPos()
	{
		auto PtrShareObject = GetStage()->GetSharedGameObject<Player>(L"Player",false);
		return PtrShareObject->GetBeginOdor();//最初の匂いを返す
	}

	//匂いとの補完距離を返す
	Vector3 Dog::GetLength()
	{
		auto PtrTrans = GetComponent<Transform>();//トランスフォーム

		auto PtrPos = PtrTrans->GetPosition();

		Odorlength = OdorPos() - PtrPos;//補完距離

		return Odorlength;
	}

	void Dog::DeleteDOdor()
	{
		auto PtrShareObject = GetStage()->GetSharedGameObject<Player>(L"Player", false);

		PtrShareObject->DeleteOdor();
	}

	//----------------------------
	//伝説のスーパーステートマシン
	//----------------------------

	//インスタンス
	shared_ptr<DefaultDogState> DefaultDogState::Instance() {
		static shared_ptr<DefaultDogState> instance;
		if (!instance)
		{
			instance = shared_ptr<DefaultDogState>(new DefaultDogState);
		}
		return instance;
	}

	void DefaultDogState::Enter(const shared_ptr<Dog>& Obj)
	{
		//することない
	}
	void DefaultDogState::Execute(const shared_ptr<Dog>& Obj)
	{
		Obj->ChaceOdor();//匂いを追う
		if ((abs(Obj->GetLength().x) < 0.5f) || (abs(Obj->GetLength().z) < 0.5f))
		{
			Obj->DeleteDOdor();
		}
	}

	void DefaultDogState::Exit(const shared_ptr<Dog>& Obj)
	{
		//触らぬ神に祟りなし
	}
}
/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player::Player(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_MaxSpeed(40.0f),	//最高速度
		m_Decel(0.95f),	//減速値
		m_Mass(1.0f),//質量
		m_MoveBase(5.0f),//移動距離の基準
		m_isReset(false)
	{}

	//初期化
	void Player::OnCreate() {
		//初期位置などの設定
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(0.25f, 0.25f, 0.25f);	//直径25センチの球体
		Ptr->SetRotation(0.0f, 0.0f, 0.0f);
		Ptr->SetPosition(0, 0.125f, 0);
		//まえのポジション
		m_BeforePos = Ptr->GetPosition();

		odorVec.emplace_back(Ptr->GetPosition());


		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();
		//反発係数は0.5（半分）
		PtrRedid->SetReflection(0.5f);
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();

		//最下地点
		PtrGravity->SetBaseY(0.125f);
		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();
		//横部分のみ反発
		PtrCol->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");
		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		//描画するテクスチャを設定
		PtrDraw->SetTextureResource(L"TRACE_TX");
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));


		//透明処理
		SetAlphaActive(true);
		auto PtrCamera = dynamic_pointer_cast<LookAtCamera>(GetStage()->GetView()->GetTargetCamera());
		if (PtrCamera) {
			//LookAtCameraに注目するオブジェクト（プレイヤー）の設定
			PtrCamera->SetTargetObject(GetThis<GameObject>());
		}



		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Player> >(GetThis<Player>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->ChangeState(DefaultState::Instance());
	}

	//移動の向きを得る
	Vector3 Player::GetAngle() {
		Vector3 Angle(0, 0, 0);
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			if (CntlVec[0].fThumbLX != 0 || CntlVec[0].fThumbLY != 0) {
				float MoveLength = 0;	//動いた時のスピード
				auto PtrTransform = GetComponent<Transform>();
				auto PtrCamera = GetStage()->GetView()->GetTargetCamera();
				//進行方向の向きを計算
				Vector3 Front = PtrTransform->GetPosition() - PtrCamera->GetEye();
				Front.y = 0;
				Front.Normalize();
				//進行方向向きからの角度を算出
				float FrontAngle = atan2(Front.z, Front.x);
				//コントローラの向き計算
				float MoveX = CntlVec[0].fThumbLX;
				float MoveZ = CntlVec[0].fThumbLY;
				//コントローラの向きから角度を計算
				float CntlAngle = atan2(-MoveX, MoveZ);
				//トータルの角度を算出
				float TotalAngle = FrontAngle + CntlAngle;
				//角度からベクトルを作成
				Angle = Vector3(cos(TotalAngle), 0, sin(TotalAngle));
				//正規化する
				Angle.Normalize();
				//Y軸は変化させない
				Angle.y = 0;
			}
		}
		return Angle;
	}


	//更新
	void Player::OnUpdate() {
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
		auto PtrTransform = GetComponent<Transform>();
		auto PtrPos = PtrTransform->GetPosition();
		if (odorVec.empty())
		{
			odorVec.emplace_back(PtrPos);
		}

	}

	void Player::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		
	}
	//ターンの最終更新時
	void Player::OnLastUpdate() {
		/*//wstring PositionStr(L"length:\t");
		//PositionStr += L"Xlen" + Util::FloatToWStr(GetBeforeLength().x, 6, Util::FloatModify::Fixed) + L",\t";
		//PositionStr += L"Zlen" + Util::FloatToWStr(GetBeforeLength().z, 6, Util::FloatModify::Fixed) + L",\t";
		//wstring odorStr(L"Odor:\t");
		//for (auto s : odorVec)
		//{
		//	odorStr += L"XOdor" + Util::FloatToWStr(s.x, 6, Util::FloatModify::Fixed) + L",\t";
		//	odorStr += L"ZOdor" + Util::FloatToWStr(s.z, 6, Util::FloatModify::Fixed) + L",\t";
		//}
		//wstring beginodorStr(L"beginOdor\t");
	
		//	beginodorStr += L"BeginOdor" + Util::FloatToWStr(GetBeginOdor().x, 6, Util::FloatModify::Fixed) + L",\t";
		//	beginodorStr += L"BeginOdor" + Util::FloatToWStr(GetBeginOdor().z, 6, Util::FloatModify::Fixed) + L",\t";
		//
		//wstring str = PositionStr+odorStr+beginodorStr;

		////デバッグ用文字をつける
		//auto PtrString = GetComponent<StringSprite>();
		//PtrString->SetText(str);*/
	}

	//補完距離を返す
	Vector3 Player::GetBeforeLength()
	{
		//lengthを核脳
		Vector3 BeforeLnegth;

		//ポジションをゲットだぜ!
		auto PtrPosition = GetComponent<Transform>()->GetPosition();

		//
		BeforeLnegth = PtrPosition - m_BeforePos;
		
		return BeforeLnegth;

	}

	bool Player::DropOdorFasible() {

	}

	//匂いを残す関数
	void Player::DropOdor()
	{
		
		auto PtrTransform = GetComponent<Transform>();
		auto PtrPos = PtrTransform->GetPosition();

		odorVec.emplace_back(PtrPos);

	}

	//モーションを実装する関数群
	//移動して向きを移動方向にする
	void Player::MoveRotationMotion() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		Vector3 Angle = GetAngle();
		//Transform
		auto PtrTransform = GetComponent<Transform>();
		//GetPosition
		auto PtrPosition = PtrTransform->GetPosition();
		//コントローラーの取得
		//auto GetCntl = App::GetApp()->GetInputDevice().GetControlerVec();

		////xとzそれぞれの移動方向の計算
		//auto Move_X = PtrPosition.x + GetCntl[0].fThumbLX*m_MoveBase*ElapsedTime;
		//auto Move_Z = PtrPosition.z + GetCntl[0].fThumbLY*m_MoveBase*ElapsedTime;

		//PtrTransform->SetPosition(Vector3(Move_X, PtrPosition.y,Move_Z ));

		//Rigidbodyを取り出す
		auto PtrRedit = GetComponent<Rigidbody>();
		//現在の速度を取り出す
		auto Velo = PtrRedit->GetVelocity();
		//目的地を最高速度を掛けて求める
		auto Target = Angle * m_MaxSpeed;
		//目的地に向かうために力のかける方向を計算する
		//Forceはフォースである
		auto Force = Target - Velo;
		//yは0にする
		Force.y = 0;
		//加速度を求める
		auto Accel = Force / m_Mass;
		//ターン時間を掛けたものを速度に加算する
		Velo += (Accel * ElapsedTime);
		//減速する
		Velo *= m_Decel;
		//速度を設定する
		PtrRedit->SetVelocity(Velo);
		//回転の計算
		float YRot = PtrTransform->GetRotation().y;
		Quaternion Qt;
		Qt.Identity();
		if (Angle.Length() > 0.0f) {
			//ベクトルをY軸回転に変換
			float PlayerAngle = atan2(Angle.x, Angle.z);
			Qt.RotationRollPitchYaw(0, PlayerAngle, 0);
			Qt.Normalize();
		}
		else {
			Qt.RotationRollPitchYaw(0, YRot, 0);
			Qt.Normalize();
		}
		//Transform
		PtrTransform->SetQuaternion(Qt);
	}

	Vector3 Player::GetBeginOdor()
	{
		vector<Vector3>::iterator odorItr=odorVec.begin();
		
		return Vector3(odorItr->x,0,odorItr->z);
	}

	//前の位置を更新
	void Player::NewBeforePos()
	{
		auto PtrTransforemPos = GetComponent<Transform>()->GetPosition();
		m_BeforePos = PtrTransforemPos;
	}

	void Player::DeleteOdor() {
		odorVec.erase(odorVec.begin());
	}

	////Aボタンでジャンプするどうかを得る
	//bool Player::IsJumpMotion() {
	//	//コントローラの取得
	//	auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
	//	if (CntlVec[0].bConnected) {
	//		//Aボタンが押された瞬間ならジャンプ
	//		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
	//			return true;
	//		}
	//	}
	//	return false;
	//}
	//Aボタンでジャンプする瞬間の処理
	//void Player::JumpMotion() {
	//	auto PtrTrans = GetComponent<Transform>();
	//	//重力
	//	auto PtrGravity = GetComponent<Gravity>();
	//	//ジャンプスタート
	//	Vector3 JumpVec(0.0f, 4.0f, 0);
	//	PtrGravity->StartJump(JumpVec, 0);
	//}
	//Aボタンでジャンプしている間の処理
	//ジャンプ終了したらtrueを返す
	//bool Player::JumpMoveMotion() {
	//	auto PtrTransform = GetComponent<Transform>();
	//	//重力
	//	auto PtrGravity = GetComponent<Gravity>();
	//	if (PtrGravity->GetGravityVelocity().Length() <= 0) {
	//		return true;
	//	}
	//	return false;
	//}


	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<DefaultState> DefaultState::Instance() {
		static shared_ptr<DefaultState> instance;
		if (!instance) {
			instance = shared_ptr<DefaultState>(new DefaultState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void DefaultState::Enter(const shared_ptr<Player>& Obj) {
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void DefaultState::Execute(const shared_ptr<Player>& Obj) {
		Obj->MoveRotationMotion();
		if ((abs(Obj->GetBeforeLength().x) > 1) || (abs(Obj->GetBeforeLength().z) > 1))
		{
			Obj->DropOdor();
			Obj->NewBeforePos();
			//Obj->DeleteOdor();
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void DefaultState::Exit(const shared_ptr<Player>& Obj) {
		//何もしない
	}




}
//end basecross

